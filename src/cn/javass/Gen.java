package cn.javass;

import cn.javass.xgen.dispatch.GenFacade;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class Gen {

	public static void main(String args[]) {
		GenFacade.generate();
		System.out.println("gen complete...");
	}
	
}
