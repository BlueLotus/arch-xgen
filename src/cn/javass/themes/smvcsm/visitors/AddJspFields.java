package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class AddJspFields implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String moduleName = ((ExtendConfModel)moduleConf.getMapExtends().get("moduleName")).getValue();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFields")).getValues();
        String domainFieldsNames[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFieldsNames")).getValues();
        StringBuffer buffer = new StringBuffer("");
        
        for(int i = 0; i < domainFields.length; i += 2)
        {
            buffer.append("<tr>\n\t");
            buffer.append((new StringBuilder("<td>")).append(domainFieldsNames[i]).append("</td>\n\t").toString());
            buffer.append((new StringBuilder("<td><input type=\"text\" name=\"")).append(domainFields[i]).append("\" class=\"input\"></td>\n\t").toString());
            if(i < domainFields.length - 1)
            {
                buffer.append((new StringBuilder("<td>")).append(domainFieldsNames[i + 1]).append("</td>\n\t").toString());
                buffer.append((new StringBuilder("<td><input type=\"text\" name=\"")).append(domainFields[i + 1]).append("\" class=\"input\"></td>\n").toString());
            }
            buffer.append("</tr>\n");
        }

        return buffer.toString();
        
    }
    
}
