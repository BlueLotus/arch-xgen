package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueryJspJson implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFields")).getValues();        
        StringBuffer buffer = new StringBuffer("var json = '{");
        
        for(int i = 0; i < domainFields.length; i++) {
        	if(i == 0) {
        		buffer.append("\"" + domainFields[i] + "\":\"' + $(\"#" + domainFields[i] + "\").val() + '\"'");
        	} else {
        		buffer.append(" + ',\"" + domainFields[i] + "\":\"' + $(\"#" + domainFields[i] + "\").val() + '\"'");
        	}
        }
        
        buffer.append(" + '}';");
        return buffer.toString();
        
    }
    
}
