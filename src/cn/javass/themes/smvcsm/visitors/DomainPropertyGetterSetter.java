package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class DomainPropertyGetterSetter implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFields")).getValues();
        String domainFieldsTypes[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFieldsTypes")).getValues();
        StringBuffer buffer = new StringBuffer("");
        
        for(int i = 0; i < domainFields.length; i++)
        {
            buffer.append((new StringBuilder("public void set")).append(domainFields[i].substring(0, 1).toUpperCase()).append(domainFields[i].substring(1)).append("(").append(domainFieldsTypes[i]).append(" obj) {\n\t\t").toString());
            buffer.append((new StringBuilder("this.")).append(domainFields[i]).append(" = obj;\n\t").toString());
            buffer.append("}\n\t");
            buffer.append((new StringBuilder("public ")).append(domainFieldsTypes[i]).append(" get").append(domainFields[i].substring(0, 1).toUpperCase()).append(domainFields[i].substring(1)).append("() {\n\t\t").toString());
            buffer.append((new StringBuilder("return this.")).append(domainFields[i]).append(";\n\t").toString());
            buffer.append("}\n\n\t");
        }

        return buffer.toString();
        
    }
    
}
