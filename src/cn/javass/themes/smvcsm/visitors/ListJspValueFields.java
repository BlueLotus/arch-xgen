package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class ListJspValueFields implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFields")).getValues();
        String moduleName = ((ExtendConfModel)moduleConf.getMapExtends().get("moduleName")).getValue();
        StringBuffer buffer = new StringBuffer("");
        
        for(int i = 0; i < domainFields.length; i++)
            buffer.append((new StringBuilder("<td>${")).append(moduleName + ".").append(domainFields[i]).append("}</td>\n\t").toString());

        buffer.append("<td>\n\t");
        buffer.append((new StringBuilder("<a href=\"${pageContext.request.contextPath}/")).append(moduleName).append("service/update/${").append(moduleName).append(".uuid}\">Modify</a> |\n\t").toString());
        buffer.append((new StringBuilder("<a href=\"${pageContext.request.contextPath}/")).append(moduleName).append("service/delete/${").append(moduleName).append(".uuid}\">Delete</a>\n\t").toString());
        buffer.append("</td>\n");
        return buffer.toString();
        
    }
    
}
