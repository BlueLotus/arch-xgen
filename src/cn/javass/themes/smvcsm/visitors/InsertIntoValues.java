package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class InsertIntoValues implements Visitor {

	@Override
	public Object visitTemplateElement(TemplateElement element) {
		
		ModuleConfModel moduleConf = element.getModuleConf();
		String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFields")).getValues();
		StringBuffer buffer = new StringBuffer("");
		
		for(int i = 0; i < domainFields.length; i++) {
			buffer.append("#{" + domainFields[i] + "}");
			if(i < domainFields.length - 1) {
				buffer.append(", ");
			}
		}
		
		return buffer.toString();
		
	}
	
}
