package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueryModelProperty implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("queryModelForDomainFields")).getValues();
        String domainFieldsTypes[] = ((ExtendConfModel)moduleConf.getMapExtends().get("queryModelForDomainFieldsTypes")).getValues();
        StringBuffer buffer = new StringBuffer("");
        
        for(int i = 0; i < domainFields.length; i++) {
        	if(domainFields[i] != null && domainFields[i].trim().length() != 0)
                buffer.append((new StringBuilder("private ")).append(domainFieldsTypes[i]).append(" ").append(domainFields[i]).append(";\n\t").toString());
        }

        return buffer.toString();
        
    }
    
}
