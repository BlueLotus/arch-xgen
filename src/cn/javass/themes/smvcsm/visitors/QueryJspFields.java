package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueryJspFields implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFields")).getValues();
        String domainFieldsTypes[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFieldsTypes")).getValues();
        String domainFieldsNames[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFieldsNames")).getValues();
        StringBuffer buffer = new StringBuffer("");
        
        int count = 0;
        for(int i = 0; i < domainFields.length; i++) {
                if(count == 0) {
                	buffer.append("<tr>\n\t");
                }

                buffer.append((new StringBuilder("<td>")).append(domainFieldsNames[i]).append("</td>\n\t").toString());
                buffer.append((new StringBuilder("<td><input type=\"text\" id=\"")).append(domainFields[i]).append("\" name=\"")
                	  .append(domainFields[i]).append("\" class=\"input\"></td>\n\t").toString());
                
                if(++count == 2) {
                    buffer.append("</tr>\n");
                    count = 0;
                }
        }
        
        /*count = 0;
        String queryModelForDomainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("queryModelForDomainFields")).getValues();
        String queryModelForDomainFieldsNames[] = ((ExtendConfModel)moduleConf.getMapExtends().get("queryModelForDomainFieldsNames")).getValues();
        for(int i = 0; i < domainFields.length; i++)
            if(isNumber(domainFieldsTypes[i]))
            {
                buffer.append("<tr>\n\t");
                int qmIndex = qmHasField((new StringBuilder(String.valueOf(domainFields[i]))).append("2").toString(), queryModelForDomainFields);
                if(qmIndex > 0)
                {
                    buffer.append((new StringBuilder("<td>")).append(domainFieldsNames[i]).append("\u5927\u4E8E\u7B49\u4E8E</td>\n\t").toString());
                    buffer.append((new StringBuilder("<td><input type=\"text\" id=\"" + domainFields[i] + "\" name=\"")).append(domainFields[i]).append("\" class=\"input\"></td>\n\t").toString());
                    buffer.append((new StringBuilder("<td>")).append(queryModelForDomainFieldsNames[qmIndex - 1]).append("</td>\n\t").toString());
                    buffer.append((new StringBuilder("<td><input type=\"text\" id=\"" + queryModelForDomainFieldsNames[qmIndex - 1] + "\" name=\"")).append(queryModelForDomainFields[qmIndex - 1]).append("\" class=\"input\"></td>\n").toString());
                } else
                {
                    buffer.append((new StringBuilder("<td>")).append(domainFieldsNames[i]).append("</td>\n\t").toString());
                    buffer.append((new StringBuilder("<td><input type=\"text\" id=\"" + domainFieldsNames[i] +"\" name=\"")).append(domainFields[i]).append("\" class=\"input\"></td>\n\t").toString());
                }
                buffer.append("</tr>\n");
            }*/

        return buffer.toString();
    }

    private int qmHasField(String fName, String qmVoFields[])
    {
        for(int i = 1; i <= qmVoFields.length; i++)
            if(qmVoFields[i - 1].equals(fName))
                return i;

        return 0;
    }

    private boolean isNumber(String type)
    {
        return "int".equals(type) || "Integer".equals(type) || "float".equals(type) || "Float".equals(type) || "double".equals(type) || "Double".equals(type);
    }
    
}
