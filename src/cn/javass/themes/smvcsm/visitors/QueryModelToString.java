package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueryModelToString implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("queryModelForDomainFields")).getValues();
        StringBuffer buffer = new StringBuffer("\"Model\"+this.getClass().getName()+\",\"+super.toString()+\" ,[");
        
        for(int i = 0; i < domainFields.length; i++) {
        	if(domainFields[i] != null && domainFields[i].trim().length() != 0)
            {
                String fName = domainFields[i];
                if(i != domainFields.length - 1) {
                	buffer.append((new StringBuilder()).append(fName).append("=\" + this.get").append(fName.substring(0, 1).toUpperCase()).append(fName.substring(1)).append("() + \",").toString());
                } else {
                	buffer.append((new StringBuilder()).append(fName).append("=\" + this.get").append(fName.substring(0, 1).toUpperCase()).append(fName.substring(1)).append("() + \"").toString());
                }
            }
        }
        
        buffer.append("]\";");
        return buffer.toString();
        
    }
    
}
