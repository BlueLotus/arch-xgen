package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class ListJspHeadFields implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFieldsNames[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFieldsNames")).getValues();
        StringBuffer buffer = new StringBuffer("");
        
        for(int i = 0; i < domainFieldsNames.length; i++)
            buffer.append((new StringBuilder("<td>")).append(domainFieldsNames[i]).append("</td>\n\t").toString());

        return buffer.toString();
        
    }
    
}
