package cn.javass.themes.smvcsm.visitors;

import cn.javass.xgen.genconf.vo.ExtendConfModel;
import cn.javass.xgen.genconf.vo.ModuleConfModel;
import cn.javass.xgen.template.visitors.TemplateElement;
import cn.javass.xgen.template.visitors.Visitor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class DomainToString implements Visitor {

    public Object visitTemplateElement(TemplateElement element) {
    	
        ModuleConfModel moduleConf = element.getModuleConf();
        String domainFields[] = ((ExtendConfModel)moduleConf.getMapExtends().get("domainFields")).getValues();
        StringBuffer buffer = new StringBuffer("\"Model\"+this.getClass().getName()+\": [");
        
        for(int i = 0; i < domainFields.length; i++)
        {
            String fieldName = domainFields[i];
            if(i != domainFields.length - 1) {
            	buffer.append((new StringBuilder()).append(fieldName).append("=\" + this.get").append(fieldName.substring(0, 1).toUpperCase()).append(fieldName.substring(1)).append("() + \",").toString());
            } else {
            	buffer.append((new StringBuilder()).append(fieldName).append("=\" + this.get").append(fieldName.substring(0, 1).toUpperCase()).append(fieldName.substring(1)).append("() + \"").toString());
            }
        }

        buffer.append("]\";");
        return buffer.toString();
        
    }
    
}
